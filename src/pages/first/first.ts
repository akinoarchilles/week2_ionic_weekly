import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SecondPage } from '../second/second';

/**
 * Generated class for the FirstPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-first',
  templateUrl: 'first.html',
})
export class FirstPage {
  SecondPage: any
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.SecondPage = SecondPage;
  }

  ionViewDidLoad() {
    console.log(this.navParams.get('nama') + ', ' + this.navParams.get('umur'));
  }

  secondPage(){
  	this.navCtrl.push(SecondPage);
  }

}
